@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <ul id="userStatus">
                        <li>Processing...</li>
                    </ul>
                    <a href="#" id="seeResults" class="hide btn btn-primary btn-xs pull-right">See results</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var statusInterval = setInterval(function(){ 
            $.get('user_status', function(res, status){
                var data = res.data;
                $("#userStatus").empty();
                $.each(data, function(i, v){
                    li = `<li>${v.status} <code>${v.processed}</code></li>`;
                    $("#userStatus").append(li);
                });
                if(res.processed === true)
                {
                    clearInterval(statusInterval);
                    console.log('All Processed !!');
                    $("#seeResults").removeClass('hide');
                }
            });
        }, 3000);
    });
</script>
@endsection
