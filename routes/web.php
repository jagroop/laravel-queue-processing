<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user_status', function(){
	$status = auth()->user()->status;
	$all = App\Status::all();
	$prev = App\Status::where('id', '<=', $status->id)->get();
	$intersectCheck = $all;
	$processed = $intersectCheck->intersect($prev)->pluck('id')->all();
	$data = $all->map(function($item, $key) use ($processed) {
		$response = [];
		$response['id'] = $item['id'];
		$response['status'] = $item['status'];
		$response['processed'] = (in_array($item['id'], $processed)) ? 'Done' : 'Pending...';
		return $response;
	});
	$allProcessed = count($data) == count($processed);
	return response()->json(['data' => $data, 'processed' => $allProcessed]);
});
