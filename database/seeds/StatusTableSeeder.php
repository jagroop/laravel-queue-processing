<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::statement('set foreign_key_checks = 0;');
    	DB::table('statuses')->truncate();
    	DB::statement('set foreign_key_checks = 1;');
        DB::table('statuses')->insert([
        	['status' => 'First'],
        	['status' => 'Second'],
        	['status' => 'Third'],
        	['status' => 'Fourth'],
        	['status' => 'Fifth'],
        ]);
    }
}
